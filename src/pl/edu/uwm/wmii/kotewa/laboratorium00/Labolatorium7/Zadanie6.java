package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium7;

public class Zadanie6 {
    public static void main(String[] args) {
        BetterRectangle prostokat1 = new BetterRectangle(2,10);
        BetterRectangle prostokat2 = new BetterRectangle(10,5);
        System.out.println("Obwod prostokata 2:  "+prostokat2.getPerimeter());
        System.out.println("Obwod prostokata 1:  "+prostokat1.getPerimeter());
        System.out.println("Pole prostokata 2:  "+prostokat2.getArea());
        System.out.println("Pole prostokata 1:  "+prostokat1.getArea());
    }
}

class BetterRectangle extends java.awt.Rectangle{
    public BetterRectangle(int height,int width){
        this.setSize(height,width);
    }
    public double getArea(){
        double pole;
        pole = width*height;
        return pole;
    }
    public double getPerimeter(){
        double obwod;
        obwod = (width+height)*2;
        return obwod;
    }
}