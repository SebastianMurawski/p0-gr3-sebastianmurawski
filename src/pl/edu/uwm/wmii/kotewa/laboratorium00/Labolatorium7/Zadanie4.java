package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium7;

public class Zadanie4 {
    public static void main(String[] args) {
        Osoba osoba1 = new Osoba("Murawski", 1999);
        Osoba osoba2 = new Osoba("Nowak", 1995);
        Student student1 = new Student("Grotto", 1990, "Informatyka");
        Student student2 = new Student("Cyrex", 1990, "Renowacja Terenow Zielonych");
        Nauczyciel nauczyciel1 = new Nauczyciel("Kazimierczak", 1978, 50000);
        Nauczyciel nauczyciel2 = new Nauczyciel("Kosmała", 1912, 50070);

        System.out.println(osoba1);
        System.out.println(osoba2);
        System.out.println(student1);
        System.out.println(student2);
        System.out.println(nauczyciel1);
        System.out.println(nauczyciel2);

        System.out.println(osoba1.getNazwisko());
        System.out.println(osoba2.getNazwisko());
        System.out.println(student1.getNazwisko());
        System.out.println(student2.getNazwisko());
        System.out.println(nauczyciel1.getNazwisko());
        System.out.println(nauczyciel2.getNazwisko());

        System.out.println(student1.getKierunek());
        System.out.println(student2.getKierunek());
        System.out.println(nauczyciel1.getPensja());
        System.out.println(nauczyciel2.getPensja());
    }
}
class Osoba {
        private String nazwisko;
        private int rok_urodzenia;

        public Osoba(String nazwisko, int rok_urodzenia) {
            this.nazwisko = nazwisko;
            this.rok_urodzenia = rok_urodzenia;
        }

        public String getNazwisko() {
            return nazwisko;
        }

        public int getRokUrodzenia() {
            return rok_urodzenia;
        }
    @Override
        public String toString() {
            return " Nazwisko: " + getNazwisko() + " Rok Urodzenia: " + getRokUrodzenia();
        }
    }
class Student extends pl.imiajd.murawski.Osoba {
        private String kierunek;
        public Student(String nazwisko, int rok_urodzenia, String kierunek) {
            super(nazwisko, rok_urodzenia);
            this.kierunek = kierunek;
        }
        public String getKierunek() {
            return kierunek;
        }
    @Override
        public String toString(){
            return super.toString() + " Kierunek: " + kierunek;
        }
    }
class Nauczyciel extends pl.imiajd.murawski.Osoba {
        private int pensja;
        public Nauczyciel(String nazwisko, int rok_urodzenia, int pensja) {
            super(nazwisko, rok_urodzenia);
            this.pensja = pensja;
        }
        public int getPensja() {
            return pensja;
        }
    @Override
        public String toString() {
            return super.toString() + " Pensja: " + pensja;
        }
    }


