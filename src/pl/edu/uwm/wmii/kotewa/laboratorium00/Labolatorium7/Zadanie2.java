package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium7;

public class Zadanie2 {
    public static void main(String[] args) {
        Zadanie2 osoba = new Zadanie2(6,"Spokojna",7,"06-400","Olsztyn");
        Zadanie2 osoba2 = new Zadanie2(6,"Spokojna",7,"06-4500","Olsztyn");
        osoba.pokaz();
        System.out.println(osoba.przed(osoba2));

    }
    private int numer_domu;
    private String ulica;
    private int numer_mieszkania;
    private String kod_pocztowy;
    private String miasto;

    public Zadanie2(int numer_domu, String ulica, int numer_mieszkania, String kod_pocztowy, String miasto) {
        this.numer_domu = numer_domu;
        this.ulica=ulica;
        this.numer_mieszkania=numer_mieszkania;
        this.kod_pocztowy=kod_pocztowy;
        this.miasto=miasto;
    }
    public Zadanie2(int numer_domu, String ulica, String kod_pocztowy, String miasto) {
        this.numer_domu = numer_domu;
        this.ulica=ulica;
        this.kod_pocztowy=kod_pocztowy;
        this.miasto=miasto;
    }
    public void pokaz(){
        System.out.println(kod_pocztowy+ "    "+miasto);
        System.out.println(ulica+"     "+numer_domu+"    "+numer_mieszkania);
    }
    public boolean przed(Zadanie2 Object){
        if(kod_pocztowy.equals(Object.kod_pocztowy)){
            return true;
        }
        else {
            return false;
        }
    }
}