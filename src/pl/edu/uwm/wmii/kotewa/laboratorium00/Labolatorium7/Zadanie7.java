package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium7;
import pl.imiajd.murawski.BetterRectangle;
public class Zadanie7 {
    public static void main(String[] args) {
        BetterRectangle prostokat1 = new BetterRectangle(2,10);
        BetterRectangle prostokat2 = new BetterRectangle(10,5);
        System.out.println("Obwod prostokata 2:  "+prostokat2.getPerimeter());
        System.out.println("Obwod prostokata 1:  "+prostokat1.getPerimeter());
        System.out.println("Pole prostokata 2:  "+prostokat2.getArea());
        System.out.println("Pole prostokata 1:  "+prostokat1.getArea());
    }
}
