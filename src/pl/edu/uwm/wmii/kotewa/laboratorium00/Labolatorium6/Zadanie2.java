package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium6;

public class Zadanie2 {
    public static void main(String[] args) {
        IntegerSet s1 = new IntegerSet();
        for(int i =0 ; i<100 ; i++){
            if(i <=8) {
                s1.set[i] = true;
            }
            else {
                s1.set[i] = false;
            }
        }
        s1.writeSet(s1.set);
        IntegerSet s2 = new IntegerSet();
        for (int i=0;i<100;i++){
            if(i<=11)
                s2.set[i]= true;
            else
                s2.set[i]=false;
        }
        s2.writeSet(s2.set);
        IntegerSet.writeSet(IntegerSet.union(s1.set,s2.set));
        IntegerSet.writeSet(IntegerSet.intersection(s1.set,s2.set));
        IntegerSet.writeSet(IntegerSet.insertElement(14,s2.set));
        IntegerSet.writeSet(IntegerSet.deleteElement(14,s2.set));
        String siema = s2.toString();
        System.out.println(siema);
        System.out.println(s1.equals(s1));
    }
}
