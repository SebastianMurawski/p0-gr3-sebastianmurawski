package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium6;

public class IntegerSet {
    boolean[] set = new boolean[100];

    public static void writeSet(boolean[] set) {
        for (int i = 0; i < 100; i++) {
            if (set[i] == true)
                System.out.print(i + 1 + " , ");
        }
        System.out.println();
    }

    public static boolean[] union(boolean[] s1, boolean[] s2) {
        boolean[] us = new boolean[100];
        for (int i = 0; i < 100; i++) {
            if (s1[i] == false && s2[i] == false) {
                us[i] = false;
            } else {
                us[i] = true;
            }
        }
        return us;
    }

    public static boolean[] intersection(boolean[] s1, boolean[] s2) {
        boolean[] us2 = new boolean[100];
        for (int i = 0; i < 100; i++) {
            if (s1[i] == false && s2[i] == false) {
                us2[i] = false;
            }if (s1[i]==s2[i]){
                us2[i]=s2[i];
            }
        }
        return us2;
    }
    public static boolean[] insertElement(int liczba,boolean[]set){
        boolean[] us3 = new boolean[100];
        int i;
        for (i = 0; i <100; i++) {
            us3[i] = set[i];
            if(i== liczba-1){
                us3[i] = true;
            }
        }
        return us3;
    }
    public static boolean[] deleteElement(int liczba,boolean[]set){
        boolean[] us4 = new boolean[100];
        int i;
        for (i = 0; i <100; i++) {
            us4[i] = set[i];
            if(i == liczba-1){
                us4[i] = false;
            }
        }
        return us4;
    }
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < 100; i++){
            if (set[i]){
                str.append(i + 1).append(" ");
            }
        }
        return str.toString();

    }
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        final IntegerSet other = (IntegerSet) obj;

        if(this.set == other.set){
            return true;
        }
        else
            return false;
    }
}

