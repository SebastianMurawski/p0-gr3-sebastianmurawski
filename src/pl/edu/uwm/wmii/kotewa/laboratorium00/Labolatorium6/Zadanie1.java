package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium6;

public class Zadanie1 {
    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy();
        saver1.saldo = 2000;
        RachunekBankowy saver2 = new RachunekBankowy();
        saver2.saldo = 3000;
        RachunekBankowy.rocznaStopaProcentowa = 0.05;
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        double wynik1 = saver1.obliczMiesieczneOdsetki();
        double wynik2 = saver2.obliczMiesieczneOdsetki();
        System.out.println("Odsetki dla pierwszego klienta wynasza: " + wynik1);
        System.out.println("Stan rachunku klienta nr1 : " + saver1.saldo);
        System.out.println("Odsetki dla drugiego klienta wynasza: " + wynik2);
        System.out.println("Stan rachunku klienta nr2 : " + saver2.saldo);

    }
}
