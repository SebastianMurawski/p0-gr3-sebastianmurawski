package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium3;

import java.util.Random;
import java.util.Scanner;

public class Lab3ex1e {
    public static void main(String[] args) {
        int liczbaN;
        Scanner input = new Scanner(System.in);


        do {
            System.out.println("\nPodaj ilosc wczytanych elementow: ");//Wczytuje liczbe N
            liczbaN = input.nextInt();
            if (liczbaN < 0)
                System.out.printf("N = %s \nN musi byc od 1 do 100",liczbaN);
        }
        while (liczbaN < 1 || liczbaN >= 101);
        System.out.println("==========================================");

        int[] tablica = new int[liczbaN];
        int minimalna = -999;
        int maksymalna = 999;
        Random random = new Random();
        int j;
        for (j = 0; j < liczbaN; j++) {//losuje liczby
            tablica[j] = (random.nextInt(maksymalna - minimalna) + minimalna);
            System.out.print(tablica[j]);
            System.out.print(" ");
        }
        System.out.println("\n==========================================");
        //obliczy długość najdłuższego fragmentu tablicy, w którym wszystkie elementy są do-datnie.
        int najStrike = 0;
        int licznik = 0;
        for(int i = 0; i<liczbaN; i++){
            if(tablica[i]>0)
                licznik++;
            if(licznik > najStrike)
                najStrike = licznik;
            if(tablica[i]<0)
                licznik = 0;
        }
        System.out.printf("\nMaksymalna dlugosc ciagu liczb dodatnich wynosi: %s.\n",najStrike);
    }
}
