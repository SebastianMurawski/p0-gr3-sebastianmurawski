package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium3;

import java.util.Random;
import java.util.Scanner;

public class Lab3ex3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m; int k; int n;

        do {
            System.out.println("Podaj wielkosc m: ");
            m = input.nextInt();
            if (m < 1 || m > 10)
                System.out.println("M musi byc z przedzialu <1,10>");
        }
        while (m < 1 || m > 10);

        do {
            System.out.println("Podaj wielkosc n: ");
            n = input.nextInt();
            if (n < 1 || n > 10)
                System.out.println("M musi byc z przedzialu <1,10>");
        }
        while (n < 1 || n > 10);

        do {
            System.out.println("Podaj wielkosc k: ");
            k = input.nextInt();
            if (k < 1 || k > 10)
                System.out.println("M musi byc z przedzialu <1,10>");
        }
        while(k < 1 || k> 10);

        int min = 1; int max = 10; int i ; int j ;

        Random losowa = new Random();

        int[][] matrixB = new int [n][k];

        System.out.println("Macierz A: ");

        for(i = 0;i<n;i++){
            for(j = 0;j<k;j++){
                matrixB[i][j] = (losowa.nextInt(max - min )+min);
                System.out.print(matrixB[i][j]+",");
            }
            System.out.println();
        }


        int[][] matrixA = new int[m][n];
        System.out.println("Macierz B: ");

        for(i = 0;i<m;i++){
            for(j = 0;j<n;j++){
                matrixA[i][j] = (losowa.nextInt(max - min )+min);
                System.out.print(matrixA[i][j]+",");
            }
            System.out.println();
        }

        System.out.println("Macierz C:");
        mnozenieMacierzy(matrixA,matrixB,m,n,k);
    }

    public static void mnozenieMacierzy(int[][] matrixA, int[][] matrixB, int m, int n, int k) {
        int i; int j; int z;

        int[][] matrixC = new int[m][k];

        for (i = 0; i < n; i++) {
            for (j = 0; j < k; j++) {
                for (z = 0; z < m; z++) {
                    matrixC[j][z] += matrixA[z][i] * matrixB[i][j];
                }
            }
        }


        for (j = 0; j < m; j++) {
            for (i = 0; i < k; i++) {
                System.out.print(matrixC[i][j] + ",");
            }
            System.out.println();
        }

    }
}
