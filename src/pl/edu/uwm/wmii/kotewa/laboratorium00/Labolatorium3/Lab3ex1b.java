package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium3;

import java.util.Random;
import java.util.Scanner;

public class Lab3ex1b {
    public static void main(String[] args) {
        int liczbaN;
        Scanner input = new Scanner(System.in);


        do {
            System.out.println("\nPodaj ilosc wczytanych elementow: ");//Wczytuje liczbe N
            liczbaN = input.nextInt();
            if (liczbaN < 0)
                System.out.printf("N = %s \nN musi byc od 1 do 100",liczbaN);
        }
        while (liczbaN < 1 || liczbaN >= 101);
        System.out.println("==========================================");

        int[] tablica = new int[liczbaN];
        int minimalna = -999;
        int maksymalna = 999;
        Random random = new Random();
        int j;
        for (j = 0; j < liczbaN; j++) {//losuje liczby
            tablica[j] = (random.nextInt(maksymalna - minimalna) + minimalna);
            System.out.print(tablica[j]);
            System.out.print(" ");
        }
        System.out.println("\n==========================================");

        int liczbyUjemne =0;//Zliczae elementy ujemne
        for(int i=0;i<liczbaN;i++){
            if(tablica[i]<0)
                liczbyUjemne++;
        }
        System.out.printf("\nIlość elementow ujemnych: %s.\n",liczbyUjemne);


        int liczbyZerowe =0;//Zlicza elementy zerowe
        for(int i=0;i<liczbaN;i++){
            if(tablica[i]==0)
                liczbyZerowe++;
        }
        System.out.printf("Ilość elementow zerowych: %s.\n",liczbyZerowe);

        int elementyDodatnie =0;//Zlicza elementy dodatnie
        for(int i=0;i<liczbaN;i++){
            if(tablica[i]>0)
                elementyDodatnie++;
        }
        System.out.printf("Ilość elementow dodatnich: %s.\n",elementyDodatnie);

    }
}
