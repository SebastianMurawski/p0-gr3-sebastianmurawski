package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium3;

import java.util.Random;
import java.util.Scanner;

public class Lab3ex1f {
    public static void main(String[] args) {
        int liczbaN;
        Scanner input = new Scanner(System.in);


        do {
            System.out.println("\nPodaj ilosc wczytanych elementow: ");//Wczytuje liczbe N
            liczbaN = input.nextInt();
            if (liczbaN < 0)
                System.out.printf("N = %s \nN musi byc od 1 do 100",liczbaN);
        }
        while (liczbaN < 1 || liczbaN >= 101);
        System.out.println("==========================================");

        int[] tablica = new int[liczbaN];
        int minimalna = -999;
        int maksymalna = 999;
        Random random = new Random();
        int j;
        for (j = 0; j < liczbaN; j++) {//losuje liczby
            tablica[j] = (random.nextInt(maksymalna - minimalna) + minimalna);
            System.out.print(tablica[j]);
            System.out.print(" ");
        }
        System.out.println("\n==========================================");
        //każdy dodatni element tablicy zastąpi liczbą1, a każdy ujemny zastąpi liczbą−1.Następnie wypisze tak zmodyfikowaną tablicę.
        System.out.println("\nPo zmianie:");
        for(int i=0 ; i<liczbaN;i++){
            if(tablica[i]<0)
                tablica[i]=-1;
            if(tablica[i]>0)
                tablica[i]=1;
            System.out.print(tablica[i]);
            System.out.print("   ");
        }
    }
}
