package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium3;

import java.util.Random;
import java.util.Scanner;

public class Lab3ex2 {
    public static void main(String[] args) {

        int liczbaN;
        Scanner input = new Scanner(System.in);
        int lewy;
        int prawy;

        do {
            System.out.print("\nPodaj ilosc wczytanych elementow: ");//Wczytuje liczbe N
            liczbaN = input.nextInt();
            if (liczbaN < 0)
                System.out.printf("N = %s \nN musi byc od 1 do 100", liczbaN);
        }
        while (liczbaN < 1 || liczbaN >= 101);

        System.out.println("==========================================");

        int[] tablica = new int[liczbaN];

        do {
            System.out.print("Podaj prawy: ");
            prawy = input.nextInt();
            if(1>prawy || prawy>=liczbaN){
                System.out.println("Prawy musi być od <1,"+liczbaN+")");
            }
        }
        while(1>prawy || prawy>=liczbaN);


        do {
            System.out.print("Podaj lewy: ");
            lewy = input.nextInt();
            if (1 > lewy || lewy >= liczbaN) {
                System.out.println("Lewy musi być od <1," + liczbaN + ")");
            }
        }
        while(1 > lewy || lewy >= liczbaN);


        if(lewy>=prawy){
            System.out.println("Lewy musi być < Prawy");
            return;
        }

        generate(tablica, liczbaN, -999, 999);
        System.out.println("\n==========================================");
        System.out.printf("\nPodpunkt A) Ile nieparzystych: %s",ileNieparzystych(tablica,liczbaN));
        System.out.printf("\nPodpunkt A) Ile parzystych: %s",ileParzystych(tablica,liczbaN));
        System.out.printf("\nPodpunkt B) Ile dodatnich: %s",ileDodatnich(tablica,liczbaN));
        System.out.printf("\nPodpunkt B) Ile ujemnych: %s",ileUjemnych(tablica,liczbaN));
        System.out.printf("\nPodpunkt B) Ile zerowych: %s",ileZerowych(tablica,liczbaN));
        System.out.printf("\nPodpunkt C) Ile wystapien: %s",ileMaksymalnych(tablica,liczbaN));
        System.out.printf("\nPodpunkt D) Suma dodatnich: %s",sumaDodatnich(tablica,liczbaN));
        System.out.printf("\nPodpunkt D) Suma ujemnych: %s",sumaUjemnych(tablica,liczbaN));
        System.out.printf("\nPodpunkt E) Dlugosc maksymalnego ciagu dodatnich liczb: %s",dlugoscMaksymalnegoCiaguDodatnich(tablica,liczbaN));
        signum(tablica,liczbaN);
        odwrocFragment(tablica,liczbaN,lewy,prawy);

    }
    public static void generate ( int[] tab, int n, int min, int max){
        for (int j = 0; j < n; j++) {
            tab[j] = getRandomNum(min, max);
            System.out.printf("%s ",tab[j]);
        }

    }
    public static int getRandomNum ( int min, int max){
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    public static int ileNieparzystych(int tablica[],int liczbaN) {
        int nieparzyste = 0;
        for (int i = 0; i < liczbaN; i++) {
            if (tablica[i] % 2 != 0)
                nieparzyste++;
        }
        return nieparzyste;

    }
    public static int ileParzystych(int tablica[], int liczbaN){
        int parzyste = 0;
        for (int i = 0; i < liczbaN; i++) {
            if (tablica[i] % 2 == 0)
                parzyste++;
        }
        return parzyste;
    }
    public static int ileDodatnich(int tablica[], int liczbaN){
        int elementyDodatnie =0;//Zlicza elementy dodatnie
        for(int i=0;i<liczbaN;i++){
            if(tablica[i]>0)
                elementyDodatnie++;
        }
        return elementyDodatnie;
    }
    public static int ileUjemnych(int tablica[], int liczbaN){
        int liczbyUjemne =0;//Zliczae elementy ujemne
        for(int i=0;i<liczbaN;i++){
            if(tablica[i]<0)
                liczbyUjemne++;
        }
        return liczbyUjemne;
    }
    public static int ileZerowych(int tablica[], int liczbaN){
        int liczbyZerowe =0;//Zlicza elementy zerowe
        for(int i=0;i<liczbaN;i++){
            if(tablica[i]==0)
                liczbyZerowe++;
        }
        return liczbyZerowe;
    }
    public static int ileMaksymalnych(int tablica[], int liczbaN){
        int ileWystapien =0;
        int maksymalna2=-999;
        for(int i=0; i<liczbaN;i++){
            if(maksymalna2<tablica[i])
                maksymalna2=tablica[i];
        }
        for(int i=0; i<liczbaN;i++){
            if(maksymalna2==tablica[i])
                ileWystapien++;
        }
        return ileWystapien;
    }
    public static int sumaDodatnich(int tablica[], int liczbaN){
        int sumaDod = 0;
        for(int i=0;i<liczbaN;i++) {
            if (tablica[i] > 0)
                sumaDod += tablica[i];
        }
        return sumaDod;
    }
    public static int sumaUjemnych(int tablica[], int liczbaN){
        int sumaUj = 0;
        for(int i=0;i<liczbaN;i++){
            if(tablica[i]<0)
                sumaUj += tablica[i];
        }
        return sumaUj;
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich(int tablica[], int liczbaN){
        int najStrike = 0;
        int licznik = 0;
        for(int i = 0; i<liczbaN; i++){
            if(tablica[i]>0)
                licznik++;
            if(licznik > najStrike)
                najStrike = licznik;
            if(tablica[i]<0)
                licznik = 0;
        }
        return najStrike;
    }
    public static void signum(int tablica[], int liczbaN){
        int[] tablica2 = new int[liczbaN];
        System.out.println("\nTablica po zmianie(signum):");
        for(int i=0; i<liczbaN;i++){
            if(tablica[i]<0)
                tablica2[i]=-1;
            if(tablica[i]>0)
                tablica2[i]=1;
            System.out.println(tablica2[i]);
        }
    }
    public static void odwrocFragment(int tablica[],int liczbaN,int lewy,int prawy){
        int y = 1;
        int x=0;
        int[] tymczasowa = new int[liczbaN];

        for(int i = lewy; i<prawy ; i++){
            tymczasowa[x]=tablica[prawy-y];
            x++;
            y++;
        }
        System.out.println("\n==========================================");
        System.out.println("Po odwróceniu: ");
        x =0;
        for(int i = lewy; i<prawy ; i++){
            tablica[i]=tymczasowa[x];
            x++;
        }
        for(int i = 0 ; i<liczbaN;i++){
            System.out.printf("%s ",tablica[i]);
        }
        System.out.println("\n==========================================");
    }

}
