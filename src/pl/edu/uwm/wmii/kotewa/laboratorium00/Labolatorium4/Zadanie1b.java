package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium4;
import java.lang.String;


public class Zadanie1b {
    public static void main(String[] args) {
        String zadanie1b = "Ala ma kota a kot ma Ale, Ale Ale Ksandra";
        String zadanie1Bdrugi = "Ale";
        System.out.println("Ilosc słow '"+zadanie1Bdrugi+"' wynosi: "+countSubStr(zadanie1b,zadanie1Bdrugi));

    }
    public static int countSubStr(String str, String subStr){
        int count =0;
        String [] splitString = str.split("[\\\\\\s@&.,?$+-]+");
        for(int i=0 ; i<splitString.length; i++){
            if(splitString[i].equals(subStr))
                count++;
        }
        return count;

    }
}
