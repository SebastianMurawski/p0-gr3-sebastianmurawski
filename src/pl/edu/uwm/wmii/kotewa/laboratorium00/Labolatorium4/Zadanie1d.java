package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium4;

import java.util.Scanner;

public class Zadanie1d {
    public static void main(String[] args) {
        String jakiesSlowo;
        int iloscPowtorzen;
        System.out.println("Podaj jakies slowo: ");
        Scanner input = new Scanner(System.in);
        jakiesSlowo = input.nextLine();
        System.out.println("Ile razy mam skopiowac");
        iloscPowtorzen = input.nextInt();
        System.out.println("Wynik: "+repeat(jakiesSlowo,iloscPowtorzen));

    }
    public static String repeat(String str, int n){
        String powtorzneie = "";
        for(int i=0 ;i<n;i++){
            powtorzneie += str;
        }
        return powtorzneie;
    }
}
