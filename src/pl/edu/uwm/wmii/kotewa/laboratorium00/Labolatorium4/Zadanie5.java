package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium4;

import java.math.BigDecimal;
import java.util.Scanner;
import java.math.*;

public class Zadanie5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj Kapitał początkowy: ");
        BigDecimal KBig = input.nextBigDecimal();
        System.out.println("Podaj ilosc kapitalizaci w ciagu roku: ");
        BigDecimal kBig = input.nextBigDecimal();
        System.out.println("Podaj oprocentowanie w skali roku: ");
        BigDecimal pBig = input.nextBigDecimal();
        System.out.println("Podaj liczbę lat oszczedzania: ");
        BigDecimal nBig = input.nextBigDecimal();
        //BigDecimal KBig = new BigDecimal("1000");
        //BigDecimal kBig = new BigDecimal("1");
        //BigDecimal pBig = new BigDecimal("4.5");
        ///BigDecimal nBig = new BigDecimal("100");
        BigDecimal potega = kBig.multiply(nBig);
        int potegaINT = potega.intValue();
        BigDecimal wynikKoncowy = pBig.divide(kBig.multiply(BigDecimal.valueOf(100))).add(BigDecimal.valueOf(1)).pow(potegaINT).multiply(KBig);
        wynikKoncowy = wynikKoncowy.setScale(2, RoundingMode.CEILING);
        System.out.println(wynikKoncowy);



    }
}
