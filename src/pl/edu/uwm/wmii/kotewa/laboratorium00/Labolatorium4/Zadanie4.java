package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium4;

import java.math.BigInteger;
import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) {
        int liczbaN;
        System.out.println("Podaj rozmiar szachowniych: ");
        Scanner input = new Scanner(System.in);
        liczbaN = input.nextInt();
        BigInteger iloscZiarenek = new BigInteger("1");
        BigInteger sumaKoncowa = new BigInteger("1");
        for(int i=1;i<liczbaN*liczbaN;i++){
            iloscZiarenek=iloscZiarenek.multiply(BigInteger.valueOf(2));
            sumaKoncowa = sumaKoncowa.add(iloscZiarenek);
            }
        System.out.println("Szachownica o rozmiarach "+liczbaN+" x "+liczbaN+" wynosi: "+sumaKoncowa);
        }
    }

