package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium4;

public class Zadanie1a {
    public static void main(String[] args) {
        String napis = "Coca-cola";
        System.out.println(countChar(napis,'c'));
    }
    public static int countChar(String str,char c){
        int count = 0;
        for(int i = 0 ; i < str.length();i++){
            if(str.charAt(i)==c){
                count +=1;
            }
        }
        return count;
    }
}
