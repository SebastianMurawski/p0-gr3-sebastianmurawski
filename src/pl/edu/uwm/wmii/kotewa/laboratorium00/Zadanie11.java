package pl.edu.uwm.wmii.kotewa.laboratorium00;

public class Zadanie11 {
    public static void main (String[] args){
        String mojUlubionyWiersz =  "      Maria Konopnicka\n"+
                                    "            Rota\n\n"+
                                    "Nie rzucim ziemi, skąd nasz ród\n"+
                                    "Nie damy pogrześć mowy!\n"+
                                    "Polski my naród, polski lud\n"+
                                    "Królewski szczep piastowy,\n"+
                                    "Nie damy by nas gnębił wróg...\n"+
                                    "Tak nam dopomóż Bóg!\n"+
                                    "Tak nam dopomóż Bóg!\n\n"+
                                    "Do krwi ostatniej kropli z żył\n"+
                                    "Bronić będziemy ducha,\n"+
                                    "Aż się rozpadnie w proch i w pył\n"+
                                    "Krzyżacka zawierucha.\n"+
                                    "Twierdzą nam będzie każdy próg.\n"+
                                    "Tak nam dopomóż Bóg!\n"+
                                    "Tak nam dopomóż Bóg!\n\n"+
                                    "Nie będzie Niemiec pluł nam w twarz,\n"+
                                    "Ni dzieci nam germanił,\n"+
                                    "Orężny wstanie hufiec nasz,\n"+
                                    "Duch będzie nam hetmanił.\n"+
                                    "Pójdziem, gdy zabrzmi złoty róg.\n"+
                                    "Tak nam dopomóż Bóg!\n"+
                                    "Tak nam dopomóż Bóg!\n\n"+
                                    "Nie damy miana Polski zgnieść,\n"+
                                    "Nie pójdziem żywo w trumnę,\n"+
                                    "W Ojczyzny imię i w jej cześć\n"+
                                    "Podnosim czoła dumne.\n"+
                                    "Odzyska ziemię dziadów wnuk.\n"+
                                    "Tak nam dopomóż Bóg!\n"+
                                    "Tak nam dopomóż Bóg!";


        System.out.println(mojUlubionyWiersz);
    }
}
