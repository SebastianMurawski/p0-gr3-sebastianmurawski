package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium13;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;




public class Zadanie4 {
    public static void randomFunctions(File file) throws FileNotFoundException {
        Map<Integer, HashSet<String>> mapa = new HashMap<>();
        Scanner input = new Scanner(file);
        while(input.hasNext()){
            String loadWord = input.next();
            Integer h = loadWord.hashCode();
            if(mapa.containsKey(h)){
                mapa.get(h).add(loadWord);
            } else {
                HashSet<String> temp = new HashSet<>();
                temp.add(loadWord);
                mapa.put(h, temp);
            }
        }
        input.close();
        for(Map.Entry<Integer, HashSet<String>> map : mapa.entrySet()){
            if(map.getValue().size() > 1){
                Iterator<String> iter = map.getValue().iterator();
                while(iter.hasNext()){
                    System.out.println("Znalezione słowo: " + iter.next()+ "\n   HASH_CODE" + map.getKey()+"\n");
                }
            }
        }
        System.out.println("Koniec....");
    }




    public static void main(String[] args) throws FileNotFoundException{
        File plik = new File("src\\pl\\edu\\uwm\\wmii\\kotewa\\laboratorium00\\Labolatorium13\\jakisTamTekst.txt");
        randomFunctions(plik);
    }

}