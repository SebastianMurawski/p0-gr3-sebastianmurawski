package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium13;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Zadanie2 {
    private static final Map<String, String> map = new TreeMap<>(String::compareTo);

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Witam w elektroniczmy dzienniku! :)\n\n");
        while (true) {
            System.out.print("Wybierz jedno wpisujac nazwe polecenia:\n1.dodaj");
            System.out.print("\n2.usun");
            System.out.print("\n3.pokaz");
            System.out.print("\n4.zmien");
            System.out.print("\n5.zakoncz\n");
            String polecenie = scanner.nextLine();
            if (polecenie.equals("dodaj")) {
                System.out.println("Jestes w panelu dodawania ocen postepuj zgodnie z instrukcja:\n");
                System.out.print("Nazwisko ucznia: ");
                String nazwisko = scanner.nextLine();
                System.out.print("Ocena ktora chcesz wstawic: ");
                String ocena = scanner.nextLine();
                map.put(nazwisko, ocena);
            }
            if (polecenie.equals("usun")) {
                System.out.println("Jestes w panelu usuwania uczniow postepuj zgodnie z instrukcja:\n");
                System.out.print("Nazwisko do usuneicia: ");
                String nazwisko = scanner.nextLine();
                map.remove(nazwisko);
            }
            if (polecenie.equals("pokaz")) {
                System.out.println("Oto twoja lista:\n");
                for (Map.Entry<String, String> o : map.entrySet()) {
                    System.out.println("Nazwisko: "+o.getKey() + "\n    Ocena: " + o.getValue());
                }
                System.out.println("\n");
            }
            if (polecenie.equals("zmien")) {
                System.out.println("Jestes w panelu zmiany nazwiska postepuj zgodnie z instrukcja:\n");
                System.out.print("Nazwisko do zmiany: ");
                String nazwisko = scanner.nextLine();
                System.out.print("Ocena: ");
                String ocena = scanner.nextLine();
                map.put(nazwisko, ocena);
            }
            if (polecenie.equals("zakoncz")) {
                System.out.println("Wyłączanie...");
                break;
            }
        }
    }
}
