package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium13;

import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Zadanie3 {
    private static final Map<Student, String> map = new TreeMap<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Wybierz jedno wpisujac nazwe polecenia:\n1.dodaj");
            System.out.print("\n2.usun");
            System.out.print("\n3.pokaz");
            System.out.print("\n4.zmien");
            System.out.print("\n5.zakoncz\n");
            String polecenie = scanner.nextLine();
            if (polecenie.equals("dodaj")) {
                System.out.println("Jestes w panelu dodawania ocen postepuj zgodnie z instrukcja:\n");
                System.out.print("Nazwisko ucznia: ");
                String nazwisko = scanner.nextLine();
                System.out.print("Imie ucznia: ");
                String imie = scanner.nextLine();
                System.out.print("Ocena ktora chcesz wstawic: ");
                String ocena = scanner.nextLine();
                map.put(new Student(nazwisko, imie), ocena);
            }
            if (polecenie.equals("usun")) {
                System.out.println("Jestes w panelu usuwania uczniow postepuj zgodnie z instrukcja:\n");
                System.out.print("Identyfikator aby usunac: ");
                Integer id = scanner.nextInt();
                usun(id);
            }
            if (polecenie.equals("pokaz")) {
                System.out.println("Oto twoja lista:\n");
                for (Map.Entry<Student, String> o : map.entrySet()) {
                    System.out.println("Nazwisko: "+o.getKey() + "\n    Ocena: " + o.getValue());
                }
            }
            if (polecenie.equals("zmien")) {
                for (Map.Entry<Student, String> o : map.entrySet()) {
                    System.out.print("Id: ");
                    String id = scanner.nextLine();
                    if (o.getKey().equals(id)) {
                        System.out.print("Ocena: ");
                        String ocena = scanner.nextLine();
                        map.put(o.getKey(), ocena);
                    }
                }
            }
            if (polecenie.equals("zakoncz")) {
                System.out.println("Wyłączanie...");
                break;
            }
        }
    }

    private static void usun(Integer id) {
        Iterator<Map.Entry<Student, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            if (it.next().getKey().equals(id)) {
                it.remove();
            }
        }
    }

    static class Student implements Comparable<Student> {
        private String imie;
        private String nazwisko;
        private Integer id;
        private static Integer iloscStudentow = 0;

        Student(String nazwisko, String imie){
            this.nazwisko = nazwisko;
            this.imie = imie;
            iloscStudentow++;
            this.id = iloscStudentow;
        }

        public boolean equals(Integer id) {
            return this.id.equals(id);
        }

        @Override
        public int compareTo(Student o) {
            if (this.nazwisko.compareTo(o.nazwisko) != 0) {
                return this.nazwisko.compareTo(o.nazwisko);
            }
            if (this.imie.compareTo(o.imie) != 0) {
                return this.imie.compareTo(o.imie);
            }
            return this.id.compareTo(o.id);
        }

        @Override
        public String toString() {
            return id + " " + imie + " " + nazwisko;
        }
    }
}
