package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium13;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class Zadanie1{
    private static final Queue<Zadanie> pq = new PriorityQueue<>();

    public static void main(String[] args){
        program();
    }

    private static void usunZadanie(){
        pq.remove();
    }

    private static void wyswietlZadania(){
        for(Zadanie zad : pq){
            System.out.println("|" + zad.getProrytet() + ". " + zad.getOpis() + "|");
        }
    }


    public static void program(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Witam oto twoj osobisty ogranizer zadan! :)\n\n");
        while(true) {

            System.out.println("1. dodaj (Prosze najpierw podac priorytet a nastepnie nazwe zadania np: dodaj 3 zakupy).");
            System.out.print("2. nastepne\n");
            System.out.print("3. zakoncz\n");
            System.out.print("4. pokaz\n");
            System.out.print("Prosze wybrac polecenie...  \n");
            String polecenie = sc.nextLine();
            String[] podzielone = polecenie.split(" ");
            if (podzielone[0].equals("dodaj")) {
                pq.add(new Zadanie(podzielone[1], podzielone[2]));
                System.out.println("\nZadanie zostalo dodane!");
                wyswietlZadania();
                System.out.println("\n");
            }

            if(polecenie.equals("nastepne")){
                System.out.println("\nJedno zadanie mniej! Oby tak dalej :)\n");
                usunZadanie();
            }

            if(polecenie.equals("pokaz")){
                wyswietlZadania();
            }

            if(polecenie.equals("zakoncz")){
                System.out.println("Wyłączanie.");
                break;
            }


        }
    }

}

class Zadanie implements Comparable<Zadanie> {
    private final String priorytet;
    private final String opis;

    Zadanie(String priorytet, String opis) {
        this.priorytet = priorytet;
        this.opis = opis;
    }

    public String getProrytet(){
        return priorytet;
    }

    public String getOpis(){
        return opis;
    }

    @Override
    public int compareTo(Zadanie o) {
        return this.priorytet.compareTo(o.priorytet);
    }
}
