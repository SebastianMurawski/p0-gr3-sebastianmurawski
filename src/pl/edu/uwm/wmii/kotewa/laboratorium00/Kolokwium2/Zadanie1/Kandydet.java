package pl.edu.uwm.wmii.kotewa.laboratorium00.Kolokwium2.Zadanie1;

public class Kandydet implements Cloneable,Comparable<Kandydet> {
    private String nazwa;
    private int wiek;
    private String wykształcony;
    private int latadoświadczenia;

    public Kandydet(String nazwa, int wiek, String wykształcony, int latadoświadczenia) {
        this.nazwa = nazwa;
        this.wiek = wiek;
        this.wykształcony = wykształcony;
        this.latadoświadczenia = latadoświadczenia;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getWiek() {
        return wiek;
    }

    public String getWykształcony() {
        return wykształcony;
    }

    public int getLatadoświadczenia() {
        return latadoświadczenia;
    }

    @Override
    public int compareTo(Kandydet o) {
        if (this.wykształcony.compareTo(o.wykształcony) == 0) {
            if (this.wiek > o.wiek) {
                return 1;
            }
            if (this.wiek < o.wiek) {
                return -1;
            }
            if (this.wiek == o.wiek) {
                if (this.latadoświadczenia > o.latadoświadczenia) {
                    return 1;
                }
                if (this.latadoświadczenia < o.latadoświadczenia) {
                    return -1;
                }
                if (this.latadoświadczenia == o.latadoświadczenia) {
                    return 0;
                }
                return 0;
            }
        }return this.wykształcony.compareTo(o.wykształcony);
    }
    @Override
    public String toString() {
        return "Kandydat:" + "\n  Imie: " + nazwa + "\n  Wiek: "  + wiek  + "\n  Wyksztalcenie: " +wykształcony +"\n  Lata doświadczenia: "+latadoświadczenia+"\n";
    }

}
