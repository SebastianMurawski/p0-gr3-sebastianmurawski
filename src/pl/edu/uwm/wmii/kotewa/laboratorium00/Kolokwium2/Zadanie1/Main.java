package pl.edu.uwm.wmii.kotewa.laboratorium00.Kolokwium2.Zadanie1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        ArrayList<Kandydet> grupa = new ArrayList<>();
        grupa.add(new Kandydet("Sebastian",22,"mistrz",1));
        grupa.add(new Kandydet("Marek",22,"licencjat",2));
        grupa.add(new Kandydet("Wojtek",32,"mistrz",2));
        grupa.add(new Kandydet("Krzysiek",22,"mistrz",2));
        grupa.add(new Kandydet("Damian",22,"mistrz",2));
        grupa.add(new Kandydet("Bartek",22,"mistrz",7));
        Rekrutacja.setDoświadczenie();
        System.out.println("Lista Kandydatow");
        for(Kandydet i : grupa){
            System.out.println(i);
        }
        Collections.sort(grupa);
        System.out.println("Lista Kandydatow po sortowaniu\n");
        for(Kandydet i : grupa){
            System.out.println(i);
        }
        System.out.println("\n\n");
        System.out.println("Wybrany kandydat: ");
        //Wybor kandydata  do wypisania
        System.out.println(grupa.get(1));
        System.out.print("Wynik: ");
        // Wybor kandydata do sprawdzenia
        System.out.println(Rekrutacja.Qualified(grupa.get(1)));
        System.out.println("\n\n");
        RecruitmentMap(grupa);

    }
    //ZADANIE 2.
    public static void RecruitmentMap(ArrayList<Kandydet>klist){
        HashMap<String,Integer> mapa = new HashMap<>();
        for (Kandydet kandydet : klist) {
            if (Rekrutacja.Qualified(kandydet)) {
                mapa.put(kandydet.getNazwa(), kandydet.getLatadoświadczenia());
            }
        }
        System.out.println("HashMap");
        System.out.println(mapa);

    }
}
