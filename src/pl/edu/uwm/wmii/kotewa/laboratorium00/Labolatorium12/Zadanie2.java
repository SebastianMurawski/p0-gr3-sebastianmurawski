package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium12;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Zadanie2 {
    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<>(List.of("Nowak","Adam","Grzelak","Grzesiek","Duda","Tusk","Kononowicz","Suchodoslki","łos","Niemiecki","Meksikano"));
        System.out.println("Lista przed metoda redukuj");
        System.out.println(Arrays.toString(pracownicy.toArray()));

        redukuj(pracownicy, 2);
        System.out.println("Lista po metodzie redukuj");
        System.out.println(Arrays.toString(pracownicy.toArray()));
    }
    public static <T> void redukuj(LinkedList<T> pracownicy, int n){
        for(int i = n - 1; i < pracownicy.size(); i += n - 1){
            pracownicy.remove(i);
        }

    }
}
