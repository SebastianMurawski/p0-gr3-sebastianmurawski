package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium12;

import java.util.Scanner;
import java.util.Stack;

public class Zadanie6 {
    public static void main(String[] args) {
        Stack<Integer> stos = new Stack<>();
        Scanner input = new Scanner(System.in);
        int liczba =0;
        System.out.println("Podaj liczbe nieujemna całkowitą");
        liczba = input.nextInt();
        while(liczba<=0) {
            System.out.println("Zła liczba podaj prawidłową");
            liczba = input.nextInt();
        }
        while(liczba > 0){
            stos.push(liczba % 10);
            liczba /= 10;
        }

        while(!stos.isEmpty()){
            System.out.print(stos.pop() + " ");
        }

    }
}
