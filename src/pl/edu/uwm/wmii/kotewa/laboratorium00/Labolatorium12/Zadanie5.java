package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium12;

import java.util.Scanner;
import java.util.Stack;

public class Zadanie5 {
    public static void main(String[] args) {
        Stack<String> stos = new Stack<>();
        Scanner input =  new Scanner(System.in);
        System.out.println("Podaj zdanie do odworcenia");
        String zdanie = input.nextLine();

        String[] podzielone = zdanie.split(" ");

        String wynik = "";

        for(int i = 0; i < podzielone.length; i++){
            if(!podzielone[i].endsWith(".")){
                podzielone[i] = Character.toLowerCase(podzielone[i].charAt(0)) + podzielone[i].substring(1);
                stos.push(podzielone[i]);
            } else {
                podzielone[i] = podzielone[i].substring(0, podzielone[i].length() - 1);
                stos.push(podzielone[i]);
                String temp = "";
                while(!stos.isEmpty()){
                    temp += stos.pop() + " ";

                }
                temp = Character.toUpperCase(temp.charAt(0)) + temp.substring(1);
                temp = temp.substring(0, temp.length() - 1);
                wynik += temp +  ". ";
            }

        }

        System.out.println(wynik);
    }
}
