package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium12;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

import static java.lang.StrictMath.sqrt;

public class Zadanie7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n =0;
        System.out.println("Podaj liczbe nieujemna całkowitą");
        n = input.nextInt();
        while(n<=0) {
            System.out.println("Zła liczba podaj prawidłową");
            n = input.nextInt();
        }
        sitoEratostenesa(n);
    }

    public static void sitoEratostenesa(int n){
        HashSet<Integer> pierwsze = new HashSet<>();
        for(int i = 2; i < n; i++){
            pierwsze.add(i);
        }

        for(int i = 2; i < Math.sqrt(pierwsze.size()); i++){
            for(int j = i; j < n; j += i){
                if(j != i){
                    pierwsze.remove(j);
                }
            }
        }
        for(Integer i : pierwsze){
            System.out.print(i + " ");
        }

    }
}
