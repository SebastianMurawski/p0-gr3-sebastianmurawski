package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium12;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Zadanie3 {
    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<>(List.of("Nowak","Adam","Grzelak","Grzesiek","Duda","Tusk","Kononowicz","Suchodoslki","łos","Niemiecki","Meksikano"));
        System.out.println("Lista przed metoda redukuj");
        System.out.println(Arrays.toString(pracownicy.toArray()));

        odwroc(pracownicy);
        System.out.println("Lista po metodzie redukuj");
        System.out.println(Arrays.toString(pracownicy.toArray()));
    }
    public static void odwroc(LinkedList<String> lista){
        LinkedList<String> tymczasowa = new LinkedList<>(lista);
        int j=0;
        for(int i=lista.size()-1;i>=0;i--) {
            lista.set(i, tymczasowa.get(j));
            j++;
        }
    }
}
