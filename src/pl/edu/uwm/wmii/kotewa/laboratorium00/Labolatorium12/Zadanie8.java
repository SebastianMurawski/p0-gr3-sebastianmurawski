package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium12;

import java.util.LinkedList;
import java.util.List;

public class Zadanie8 {
    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<>(List.of("Nowak", "Adam", "Grzelak", "Grzesiek", "Duda", "Tusk", "Kononowicz", "Suchodoslki", "łos", "Niemiecki", "Meksikano"));
        print(pracownicy);
    }
    public static <E extends Iterable<?>> void print(E arg) {
        for (Object o : arg) {
            System.out.print(o + ", ");
        }
    }
}
