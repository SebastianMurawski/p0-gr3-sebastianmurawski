package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium8;
import java.time.LocalDate;
public class Zadanie1
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Jan Kowalski", 50000,new String[] {"Jan","Waładysław"},LocalDate.of(1975,11,20),true,LocalDate.of(1990,12,20));
        ludzie[1] = new Student("Małgorzata Nowak", new String[] {"Małgorzata","Katarzyna"},LocalDate.of(2005,12,30),false,"Informatyka",4.3);


        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
    }
}

abstract class Osoba
{
    public Osoba(String nazwisko,String[] imiona, boolean plec, LocalDate dataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.plec = plec;
        this.dataUrodzenia=dataUrodzenia;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }
    public String getImiona(){
        return imiona[0]+" "+imiona[1];
    }
    public String getPlec(){
        if(plec){
            return "Mężczyzna";
        }else{
            return "Kobieta";
        }
    }
    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }


    private String nazwisko;
    private String[] imiona;
    private boolean plec;
    private LocalDate dataUrodzenia;
}

class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, double pobory,String[] imiona,LocalDate dataUrodzenia,boolean plec, LocalDate dataZatrudnienia )
    {
        super(nazwisko,imiona,plec,dataUrodzenia);
        this.pobory = pobory;
        this.dataZatrudnienia=dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("\n    pracownik z pensją %.2f zł\n    Imiona: %s \n    Data urodzenia: %s\n    Płeć: %s \n    Data zatrudnienia: %s", getPobory(),getImiona(),getDataUrodzenia(),getPlec(),getDataZatrudnienia());
    }
    public LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}


class Student extends Osoba
{
    public Student(String nazwisko,String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko,imiona,plec,dataUrodzenia);
        this.kierunek = kierunek;
        this.sredniaOcen=sredniaOcen;
    }

    public String getOpis()
    {
        return "\n    Kierunek studiów : " + kierunek+ "\n    Srednia ocen: "+getSredniaOcen()+"\n    Płeć: " +getPlec() +"\n    Kierunek studiow: "+getKierunek()+"\n    Data Urodzenia: "+getDataUrodzenia();
    }
    public double getSredniaOcen(){
        return sredniaOcen;
    }
    public void setSredniaOcen(double sredniaOcen){
        this.sredniaOcen=sredniaOcen;
    }
    public String getKierunek(){
        return kierunek;
    }

    private String kierunek;
    private double sredniaOcen;
}

