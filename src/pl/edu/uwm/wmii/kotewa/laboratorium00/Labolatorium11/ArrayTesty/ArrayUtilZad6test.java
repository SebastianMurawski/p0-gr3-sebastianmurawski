package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium11.ArrayTesty;

import pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium11.ArrayUtil;

import java.time.LocalDate;

public class ArrayUtilZad6test {
    public static void main(String[] args) {
        LocalDate[] tab3 = new LocalDate[5];
        tab3[0] = LocalDate.of(1999, 10, 2);
        tab3[1] = LocalDate.of(1998, 9, 1);
        tab3[2] = LocalDate.of(1997, 10, 1);
        tab3[3] = LocalDate.of(1996, 10, 1);
        tab3[4] = LocalDate.of(1995, 10, 1);

        System.out.println("\nSortowanie mergeSOrt...\n");
        ArrayUtil.mergeSort(tab3);
        for (LocalDate a : tab3) {
            System.out.print(a + " ");

        }
    }
}
