package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium11;

public class PairUtil<T> {
    public PairUtil() {
        first = null;
        second = null;
    }

    public PairUtil (T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }
    public T getSecond() {
        return second;
    }

    public void setFirst (T newValue) {
        first = newValue;
    }
    public void setSecond (T newValue) {
        second = newValue;
    }

    private T first;
    private T second;
    public static <T> Pair<T> swap(Pair<T> ob) {
        return new Pair<>(ob.getSecond(), ob.getFirst());
    }
}
