package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium11;

import java.util.Arrays;

public class ArrayUtil {
    public static <T extends Comparable<T>> boolean isSorted(T[] obj) {
        int mon = 0;
        for (int i = 1; i < obj.length; i++) {
            if (obj[i - 1].compareTo(obj[i]) > 0) { mon = 1;
            } else { mon = -1; }
        }
        if (mon == 1) { return false;
        } else { return true; }
    }





    public static <T extends Comparable<T>> int binSearch(T[] obj, T searchObj) {
        int arr1 = 0;
        int arr2 = obj.length;
        while (arr1 <= arr2) {
            int middle = arr1 + (arr2 - arr1) / 2;
            if (obj[middle].compareTo(searchObj) == 0) { return middle; }
            if (obj[middle].compareTo(searchObj) < 0) { return arr1 = middle + 1; }
            if (obj[middle].compareTo(searchObj) > 0) { return arr2 = middle - 1; }
        }
        return -1;
    }



    public static <T extends Comparable<T>> void selectionSort(T[] obj) {
        for (int i = 0; i < obj.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < obj.length; j++) {
                if (obj[min].compareTo(obj[j]) > 0) {
                    min = j;
                }
            }
            T tempObj = obj[i];
            obj[i] = obj[min];
            obj[min] = tempObj;
        }
    }



    public static <T extends Comparable<T>> void mergeSort(T[] obj) {
        if (obj.length <= 1) return;
        int middle = obj.length / 2;
        T[] arr1 = Arrays.copyOfRange(obj, 0, middle);
        T[] arr2 = Arrays.copyOfRange(obj, middle, obj.length);
        mergeSort(arr1);
        mergeSort(arr2);
        int zmienna1 = 0;
        int zmienna2 = 0;
        int zmienna3 = 0;

        while (zmienna1 < arr1.length && zmienna2 < arr2.length) {
            if (arr1[zmienna1].compareTo(arr2[zmienna2]) < 0) {
                obj[zmienna3] = arr1[zmienna1];
                zmienna1++;
            } else {
                obj[zmienna3] = arr2[zmienna2];
                zmienna2++;
            }
            zmienna3++;
        }

        while (zmienna1 < arr1.length) {

            obj[zmienna3] = arr1[zmienna1];
            zmienna1++;
            zmienna3++;
        }

        while (zmienna2 < arr2.length) {

            obj[zmienna3] = arr2[zmienna2];
            zmienna2++;
            zmienna3++;
        }
    }
}

