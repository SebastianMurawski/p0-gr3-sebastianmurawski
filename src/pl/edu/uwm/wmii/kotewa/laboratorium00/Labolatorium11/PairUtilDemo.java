package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium11;

public class PairUtilDemo {
    public static void main(String[] args) {
        Pair<String> para1 = new Pair<String>("First","Second");
        System.out.println(para1.getFirst());
        System.out.println(para1.getSecond());
        para1=PairUtil.swap(para1);
        System.out.println(para1.getFirst());
        System.out.println(para1.getSecond());
    }

}
