package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium11.ArrayTesty;

import pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium11.ArrayUtil;

import java.time.LocalDate;

public class ArrayUtilZad5test {
    public static void main(String[] args) {
        LocalDate[] tab6 = new LocalDate[5];
        tab6[0] = LocalDate.of(2023, 10, 1);
        tab6[1] = LocalDate.of(200, 10, 1);
        tab6[2] = LocalDate.of(2666, 10, 1);
        tab6[3] = LocalDate.of(2003, 6, 1);
        tab6[4] = LocalDate.of(2020, 10, 1);

        System.out.println("\n\nSortowanie...");
        ArrayUtil.selectionSort(tab6);
        for (LocalDate a : tab6) {
            System.out.print(a + " ");
        }
    }
}
