package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium11.ArrayTesty;

import pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium11.ArrayUtil;

import java.time.LocalDate;

public class ArrayUtilZad3test {
    public static void main(String[] args) {
        Integer[] tab = {10, 10, 10, 2, 5, 3};

        for (Integer a : tab) {
            System.out.print(a + " ");

        }
        System.out.println("\nCzy jest uporządkowana niemalejaco: " + ArrayUtil.isSorted(tab));
        System.out.println();
        Integer[] tab2 = {1, 1, 1, 1, 1, 3};
        for (Integer a : tab2) {
            System.out.print(a + " ");

        }
        System.out.println("\nCzy jest uporządkowana niemalejaco: " + ArrayUtil.isSorted(tab2));
        System.out.println();
        LocalDate[] tab3 = new LocalDate[5];
        tab3[0] = LocalDate.of(1999, 10, 2);
        tab3[1] = LocalDate.of(1998, 9, 1);
        tab3[2] = LocalDate.of(1997, 10, 1);
        tab3[3] = LocalDate.of(1996, 10, 1);
        tab3[4] = LocalDate.of(1995, 10, 1);


        for (LocalDate a : tab3) {
            System.out.print(a + " ");

        }
        System.out.println("\nCzy jest uporządkowana niemalejaco: " + ArrayUtil.isSorted(tab3));
        System.out.println();
        LocalDate[] tab4 = new LocalDate[5];
        tab4[0] = LocalDate.of(1999, 10, 1);
        tab4[1] = LocalDate.of(2000, 10, 1);
        tab4[2] = LocalDate.of(2001, 10, 1);
        tab4[3] = LocalDate.of(2002, 10, 1);
        tab4[4] = LocalDate.of(2002, 10, 1);
        for (LocalDate a : tab4) {
            System.out.print(a + " ");

        }
        System.out.println("\nCzy jest uporządkowana niemalejaco: " + ArrayUtil.isSorted(tab4));
        System.out.println();
        LocalDate[] tab5 = new LocalDate[5];
        tab5[0] = LocalDate.of(2000, 10, 1);
        tab5[1] = LocalDate.of(2000, 10, 1);
        tab5[2] = LocalDate.of(2000, 10, 1);
        tab5[3] = LocalDate.of(2000, 10, 1);
        tab5[4] = LocalDate.of(2000, 10, 1);

        for (LocalDate a : tab5) {
            System.out.print(a + " ");

        }
        System.out.println("\nCzy jest uporządkowana niemalejaco: " + ArrayUtil.isSorted(tab5));
        System.out.println();
    }
}