package pl.edu.uwm.wmii.kotewa.laboratorium00.Essa;


import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        ArrayList <Autor> lista = new ArrayList<>();
        lista.add(new Autor("Nowak","Nowak@gmail.com",'K'));
        lista.add(new Autor("Wiśniewska","Wiśniewska12@gmail.com",'K'));
        lista.add(new Autor("Kowalski","kowalski12@gmail.com",'M'));
        lista.add(new Autor("Kowalski","kowalski@gmail.com",'M'));
        System.out.println("lista:\n");
        for(Autor i : lista) {
            System.out.println(i);
        }

        Collections.sort(lista);
        System.out.println("\nNasza lista po sortowaniu wyglada tak:\n");
        for(Autor i : lista) {
            System.out.println(i);
        }

        ArrayList <Ksiazka> ListaKsiążek = new ArrayList<>();
        ListaKsiążek.add(new Ksiazka("Krzyżacy","Sienkiewicz",20.0));
        ListaKsiążek.add(new Ksiazka("Potop","Sienkiewicz",25.0));
        ListaKsiążek.add(new Ksiazka("Granica","Nałkowska",20.0));
        ListaKsiążek.add(new Ksiazka("Nad Niemnem","Orzeszkowa",20.0));
        System.out.println("lista:\n");
        for(Ksiazka i : ListaKsiążek) {
            System.out.println(i);
        }
        Collections.sort(ListaKsiążek);
        System.out.println("\nNasza lista po sortowaniu wyglada tak:\n");
        for(Ksiazka i : ListaKsiążek) {
            System.out.println(i);
        }
    }
}
