package pl.edu.uwm.wmii.kotewa.laboratorium00.Essa;

import java.util.LinkedList;

public class Ksiazka implements Cloneable,Comparable<Ksiazka> {
    private String tytul;
    private String autor;
    private double cena;

    @Override
    public String toString() {
        return "Ksiazka{" +
                "tytul='" + tytul + '\'' +
                ", autor='" + autor + '\'' +
                ", cena=" + cena +
                '}';
    }

    public Ksiazka(String tytul, String autor, Double cena) {
        this.tytul = tytul;
        this.autor = autor;
        this.cena = cena;
    }

    @Override
    public int compareTo(Ksiazka o) {
        if(this.tytul.compareTo(o.tytul)==0){
            if(this.autor.compareTo(o.autor)==0){
                if(this.cena>o.cena){
                    return 1;
                }if(this.cena<o.cena) {
                    return -1;
                }else{
                    return 0;
                }
            }
        }
        return this.tytul.compareTo(o.tytul);
    }
}
