package pl.edu.uwm.wmii.kotewa.laboratorium00.Essa;

import java.util.Objects;

public class Autor implements Cloneable,Comparable<Autor> {
    private String nazwa;
    private String e_mail;
    private char plec;

    public Autor(String nazwa, String e_mail, char plec) {
        this.nazwa = nazwa;
        this.e_mail = e_mail;
        this.plec = plec;
    }

    @Override
    public int compareTo(Autor o) {
        if(this.nazwa.compareTo(o.nazwa)==0){
            if(this.plec<o.plec){
                return -1;
            }
            if(this.plec>o.plec){
                return 1;
            }else{
                return 0;
            }
        }
        return this.nazwa.compareTo(o.nazwa);
    }

    @Override
    public String toString() {
        return "Autor|" + "nazwisko= " + nazwa + ',' + "email= " + e_mail + ',' + "plec= " + plec + '|';
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getE_mail() {
        return e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    public char getPlec() {
        return plec;
    }

    public void setPlec(char plec) {
        this.plec = plec;
    }
}
