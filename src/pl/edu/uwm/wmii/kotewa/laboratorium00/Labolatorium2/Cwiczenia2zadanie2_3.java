package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium2;

import java.util.Scanner;

public class Cwiczenia2zadanie2_3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int liczbaN;
        int dodatnie = 0;
        int zero = 0;
        int ujemne = 0;
        System.out.println("Podaj n: ");
        liczbaN = input.nextInt();
        double liczby[];
        liczby = new double[liczbaN];
        for(int i=0; i<liczbaN ; i++){
            System.out.println("Podaj kolejna liczbe: ");
            liczby[i] = input.nextDouble();
        }
        System.out.println("\n");
        for(int i=0; i<liczbaN ; i++) {
            if(liczby[i]>0)
                dodatnie++;
            if(liczby[i]==0)
                zero++;
            if(liczby[i]<0)
                ujemne++;

        }
        System.out.println("Ilosc liczb dodatnich: "+dodatnie);
        System.out.println("Ilosc liczb ujemnych: "+ujemne);
        System.out.println("Ilosc liczb o wartosci 0: "+zero);
    }
}
