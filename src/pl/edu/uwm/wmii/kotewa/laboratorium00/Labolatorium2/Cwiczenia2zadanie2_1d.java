package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium2;

import java.util.Scanner;
public class Cwiczenia2zadanie2_1d {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int liczbaN;
        int ile_liczb=0;
        System.out.println("Podaj n: ");
        liczbaN = input.nextInt();
        int liczby[];
        liczby = new int[liczbaN];
        for(int i=0; i<liczbaN ; i++){
            System.out.println("Podaj kolejna liczbe: ");
            liczby[i] = input.nextInt();
        }
        System.out.println("\n");
        for(int i=2; i<liczbaN-1 ; i++) {
            if(liczby[i]<(liczby[i-1]+liczby[i+1])/2)
                ile_liczb++;
        }
        System.out.println("Liczby: "+ile_liczb);
    }
}
