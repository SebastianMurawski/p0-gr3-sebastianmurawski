package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium2;

import java.util.Scanner;

public class Cwiczenia2zadanie2_1g {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int liczbaN;
        int ile_liczb=0;
        System.out.println("Podaj n: ");
        liczbaN = input.nextInt();
        double liczby[];
        liczby = new double[liczbaN];
        for(int i=0; i<liczbaN ; i++){
            System.out.println("Podaj kolejna liczbe: ");
            liczby[i] = input.nextDouble();
        }
        System.out.println("\n");
        for(int i=0; i<liczbaN ; i++) {
            if(liczby[i]%2==1 && liczby[i]>=0)
                ile_liczb+=1;
        }
        System.out.println(ile_liczb);
    }
}
