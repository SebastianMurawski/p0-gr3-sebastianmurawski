package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium2;

import java.util.Scanner;

public class Cwiczenia2zadanie2_2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int liczbaN;
        int suma=0;
        System.out.println("Podaj n: ");
        liczbaN = input.nextInt();
        int liczby[];
        liczby = new int[liczbaN];
        for(int i=0; i<liczbaN ; i++){
            System.out.println("Podaj kolejna liczbe: ");
            liczby[i] = input.nextInt();
        }
        System.out.println("\n");
        for(int i=0; i<liczbaN ; i++) {
            if(liczby[i]>0)
               suma = suma + liczby[i];
        }
        suma = suma*2;
        System.out.println(suma);
    }
}
