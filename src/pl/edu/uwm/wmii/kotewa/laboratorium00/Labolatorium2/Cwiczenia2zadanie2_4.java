package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium2;

import java.util.Scanner;

public class Cwiczenia2zadanie2_4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int liczbaN;
        double min = 0;
        double max = 0;
        System.out.println("Podaj n: ");
        liczbaN = input.nextInt();
        double liczby[];
        liczby = new double[liczbaN];
        for (int i = 0; i < liczbaN; i++) {
            System.out.println("Podaj kolejna liczbe: ");
            liczby[i] = input.nextDouble();
        }
        min = liczby[0];
        System.out.println("\n");
        for (int i = 0; i < liczbaN; i++) {
            if(max<liczby[i])
                max = liczby[i];
            if(min>liczby[i])
                min = liczby[i];
        }
        System.out.println("Najmniejsza liczba to: "+min);
        System.out.println("Najwieksza liczba to: "+max);
    }
}
