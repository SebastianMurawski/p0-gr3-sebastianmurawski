package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium2;

import java.util.Scanner;

public class Cwiczenia2zadanie2_1b {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int liczbaN;
        int ile_liczb=0;
        System.out.println("Podaj n: ");
        liczbaN = input.nextInt();
        int liczby[];
        liczby = new int[liczbaN];
        for(int i=0; i<liczbaN ; i++){
            System.out.println("Podaj kolejna liczbe: ");
            liczby[i] = input.nextInt();
        }
        System.out.println("\n");
        for(int i=0; i<liczbaN ; i++){
            if(liczby[i]%3==0 && liczby[i]%5!=0 )
                ile_liczb+=1;
        }
        System.out.println(ile_liczb);
    }
}
