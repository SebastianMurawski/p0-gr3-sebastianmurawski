package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium2;

import java.util.Scanner;

public class Cwiczenia2zadania2_5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int liczbaN;
        System.out.println("Podaj n: ");
        liczbaN = input.nextInt();
        double liczby[];
        liczby = new double[liczbaN];
        for (int i = 0; i < liczbaN; i++) {
            System.out.println("Podaj kolejna liczbe: ");
            liczby[i] = input.nextDouble();
        }
        System.out.println("\n");
        for (int i = 0; i < liczbaN-1; i++) {
            if(liczby[i]>0 && liczby[i+1]>0)
                System.out.println("("+liczby[i]+","+liczby[i+1]+")");
            if(liczby[i]<0 && liczby[i+1]<0)
                System.out.println("("+liczby[i]+","+liczby[i+1]+")");

        }
    }
}
