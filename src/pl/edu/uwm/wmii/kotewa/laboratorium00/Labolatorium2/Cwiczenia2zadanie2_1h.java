package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium2;

import java.util.Scanner;
import java.lang.Math;
public class Cwiczenia2zadanie2_1h {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int liczbaN;
        int ile_liczb = 0;
        System.out.println("Podaj n: ");
        liczbaN = input.nextInt();
        double liczby[];
        liczby = new double[liczbaN];
        for (int i = 0; i < liczbaN; i++) {
            System.out.println("Podaj kolejna liczbe: ");
            liczby[i] = input.nextDouble();
        }
        System.out.println("\n");
        for (int i = 0; i < liczbaN; i++) {
            liczby[i] = Math.abs(liczby[i]);
            if (Math.pow(i, 2) > liczby[i])
                ile_liczb++;
        }

        System.out.println(ile_liczb);
    }
    }
