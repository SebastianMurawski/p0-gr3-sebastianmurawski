package pl.edu.uwm.wmii.kotewa.laboratorium00.PrzygotowanieDoKolokwiu;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        ArrayList<Komputer> lista = new ArrayList<>();
        lista.add(new Komputer("Acer",LocalDate.of(2020,12,13)));
        lista.add(new Komputer("Acer",LocalDate.of(2017,2,03)));
        lista.add(new Komputer("Apple",LocalDate.of(2011,5,13)));
        lista.add(new Komputer("Samsung",LocalDate.of(2011,5,13)));
        lista.add(new Komputer("Xiaomi",LocalDate.of(2012,12,14)));
        System.out.println("Nasza lista wyglada tak:");
        for(Komputer i : lista){
            System.out.println(i);
        }
        Collections.sort(lista);
        System.out.println("Nasza lista po sortowaniu wyglada tak:");
        for(Komputer i : lista){
            System.out.println(i);
        }

        ArrayList<Laptop> grupaLaptopow = new ArrayList<>();
        grupaLaptopow.add(new Laptop("Apple",LocalDate.of(2020,12,01),true));
        grupaLaptopow.add(new Laptop("Apple",LocalDate.of(2019,11,11),true));
        grupaLaptopow.add(new Laptop("Samsung",LocalDate.of(2020,12,01),false));
        grupaLaptopow.add(new Laptop("Xiaomi",LocalDate.of(2020,12,01),false));
        grupaLaptopow.add(new Laptop("Asus",LocalDate.of(2011,11,01),false));
        System.out.println("Nasza grupaLaptopow wyglada tak:");
        for(Laptop i : grupaLaptopow){
            System.out.println(i);
        }
        Collections.sort(grupaLaptopow);
        System.out.println("Nasza grupaLaptopw po sortowaniu wyglada tak:");
        for(Laptop i : grupaLaptopow){
            System.out.println(i);
        }




    }
}
