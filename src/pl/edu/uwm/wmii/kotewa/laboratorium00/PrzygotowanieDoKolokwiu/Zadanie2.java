package pl.edu.uwm.wmii.kotewa.laboratorium00.PrzygotowanieDoKolokwiu;

import java.util.Arrays;
import java.util.LinkedList;

public class Zadanie2 {
    public static void main(String[] args) {
        LinkedList<String> komputery = new LinkedList<>();
        komputery.add("asus");
        komputery.add("samsung");
        komputery.add("xiaomi");
        komputery.add("gigabyte");
        komputery.add("lg");
        komputery.add("lenovo");
        komputery.add("apple");
        komputery.add("alienware");
        komputery.add("komputrex");
        System.out.println("Wyglad listy");
        System.out.println(Arrays.toString(komputery.toArray()));
        System.out.println("Wyglad listy po redukcji");
        redukuj(komputery,2);
        System.out.println(Arrays.toString(komputery.toArray()));

    }



    public static void redukuj(LinkedList<String> komputery, int n) {
        for (int i = n - 1; i < komputery.size(); i += n - 1) {
            komputery.remove(i);
        }
    }
}
