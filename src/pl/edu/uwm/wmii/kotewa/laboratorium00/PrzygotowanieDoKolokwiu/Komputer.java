package pl.edu.uwm.wmii.kotewa.laboratorium00.PrzygotowanieDoKolokwiu;


import java.time.LocalDate;


public class Komputer implements Cloneable,Comparable<Komputer> {
    private String nazwa;
    private LocalDate dataProdukcji;

    public Komputer(String nazwa, LocalDate dataProdukcji){
        this.nazwa = nazwa;
        this.dataProdukcji = dataProdukcji;
    }
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public LocalDate getDataProdukcji() {
        return dataProdukcji;
    }

    public void setDataProdukcji(LocalDate dataProdukcji) {
        this.dataProdukcji = dataProdukcji;
    }

    public boolean equals(Object Obiekt){
        Komputer temp = (Komputer) Obiekt;
        if(this.nazwa.equals(temp.nazwa)&& this.dataProdukcji.equals(temp.dataProdukcji)){
            return true;
        }
        return false;
    }
    public int compareTo(Komputer Obiekt){
        if(this.nazwa.compareTo(Obiekt.nazwa)==0){
            return this.dataProdukcji.compareTo(Obiekt.dataProdukcji);
        }
        return this.nazwa.compareTo(Obiekt.nazwa);
    }
    public String toString() {
        String temp = String.format(" [Urzadzenie: %s  %s]",getNazwa(),getDataProdukcji());
        return temp;
    }
}
