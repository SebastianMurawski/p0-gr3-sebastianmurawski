package pl.edu.uwm.wmii.kotewa.laboratorium00.PrzygotowanieDoKolokwiu;

import java.time.LocalDate;

public class Laptop extends Komputer implements Cloneable,Comparable<Komputer> {
    private boolean czyApple;
    public Laptop(String nazwa, LocalDate dataProdukcji,boolean czyApple){
        super(nazwa,dataProdukcji);
        this.czyApple = czyApple;
    }

    public boolean isCzyApple()
    {
        if(czyApple==true){
            return true;
        }
        else return false;
    }

    @Override
    public String toString() {
        String temp = String.format(" [Nazwisko: %s  %s  %s]",getNazwa(),getDataProdukcji(),isCzyApple());
        return temp;
    }
    public void setCzyApple(boolean czyApple) {
        this.czyApple = czyApple;
    }


}
