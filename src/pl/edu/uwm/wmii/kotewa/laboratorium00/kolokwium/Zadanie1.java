package pl.edu.uwm.wmii.kotewa.laboratorium00.kolokwium;

import java.util.Scanner;

public class Zadanie1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int liczba;
        System.out.println("Podaj liczbe naturalna");
        liczba = input.nextInt();
        int[] tablica = new int[liczba];
        for (int i = 0; i < liczba; i++) {
            System.out.println("Prosze podac wartosc: ");
            tablica[i] = input.nextInt();
        }
        for (int i = 0; i < tablica.length; i++) {
            boolean liczbaPierwsza = true;
            if (tablica[i] == 1)
                liczbaPierwsza = false;
            else {
                for (int j = 2; j <= tablica[i] / 2; j++) {
                    if (tablica[i] % j == 0) {
                        liczbaPierwsza = false;
                        break;
                    }
                }
            }
            if (liczbaPierwsza)
                System.out.println(tablica[i]);
        }
    }
}
