package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium5;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) {
        int tablicaDlug1=0;
        int tablicaDlug2=0;
        Scanner input = new Scanner(System.in);
        System.out.println("Prosze podac dlugosc pierewszej tablicy: ");
        tablicaDlug1 = input.nextInt();
        System.out.println("Prosze podac dlugosc drugiej tablicy: ");
        tablicaDlug2 = input.nextInt();
        ArrayList<Integer> arr1 = new ArrayList<Integer>(8);
        for (int i = 0; i < tablicaDlug1; i++) {
            arr1.add(i, i + 1);
        }
        ArrayList<Integer> arr2 = new ArrayList<Integer>(3);
        for (int i = 0; i < tablicaDlug2; i++) {
            arr2.add(i, arr1.size() + i + 1);
        }
        System.out.println("Pierwsza tablica:");
        System.out.println(arr1);
        System.out.println("Drugra tablica");
        System.out.println(arr2);
        System.out.println("Tablica co sortowaniu i zlaczeniu:");
        System.out.println(mergeSorted(arr1,arr2));
    }
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> arr1, ArrayList<Integer> arr2){
        for (int index1 = 0, index2 = 0; index2 < arr2.size(); index1++) {
            if (index1 == arr1.size() || arr1.get(index1) > arr2.get(index2)) {
                arr1.add(index1, arr2.get(index2++));
            }
        }
        return arr1;
    }
}
