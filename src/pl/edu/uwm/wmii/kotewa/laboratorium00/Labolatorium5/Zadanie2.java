package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium5;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) {
        int tablicaDlug1=0;
        int tablicaDlug2=0;
        Scanner input = new Scanner(System.in);
        System.out.println("Prosze podac dlugosc pierewszej tablicy: ");
        tablicaDlug1 = input.nextInt();
        System.out.println("Prosze podac dlugosc drugiej tablicy: ");
        tablicaDlug2 = input.nextInt();
        ArrayList<Integer> a = new ArrayList<Integer>(8);
        for (int i = 0; i < tablicaDlug1; i++) {
            a.add(i, i + 1);
        }
        ArrayList<Integer> b = new ArrayList<Integer>(3);
        for (int i = 0; i < tablicaDlug2; i++) {
            b.add(i, a.size() + i + 1);
        }
        System.out.println(a);
        System.out.println(b);
        System.out.println(merge(a, b));
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> arr1, ArrayList<Integer> arr2) {
        ArrayList<Integer> newArray = new ArrayList<Integer>();
        int y = 0;
        if (arr1.size() > arr2.size()) {
            for (int x = 0; x < arr2.size(); x++) {
                newArray.add(y, arr1.get(x));
                y++;
                newArray.add(y, arr2.get(x));
                y++;
            }
            y = arr2.size();
            for (int x = arr2.size() * 2; x < arr2.size() + arr1.size(); x++) {
                newArray.add(x, arr1.get(y));
                y++;
            }
        } else {
            for (int x = 0; x < arr1.size(); x++) {
                newArray.add(y, arr2.get(x));
                y++;
                newArray.add(y, arr1.get(x));
                y++;
            }
            y = arr1.size();
            for (int x = arr1.size() * 2; x < arr1.size() + arr2.size(); x++) {
                newArray.add(x, arr2.get(y));
                y++;
            }
        }
        return newArray;
    }
}

