package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium5;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) {
        int tablicaDlug1=0;
        Scanner input = new Scanner(System.in);
        System.out.println("Prosze podac dlugosc pierewszej tablicy: ");
        tablicaDlug1 = input.nextInt();
        ArrayList<Integer> arr1 = new ArrayList<Integer>(8);
        for (int i = 0; i < tablicaDlug1; i++) {
            arr1.add(i, i + 1);
        }
        System.out.println("Poczatkowy wyglad tablicy: ");
        System.out.println(arr1);
        System.out.println("Tablica odwrocona: ");
        System.out.println(reversed(arr1));

    }
    public static ArrayList<Integer> reversed(ArrayList<Integer> arr1){
        ArrayList<Integer> odwrocona = new ArrayList<Integer>();
        int rozmiar = arr1.size();
        int j =rozmiar-1;
        for(int i=0;i<rozmiar;i++){
            odwrocona.add(i, arr1.get(j));
            j--;
        }
        return odwrocona;
    }
    }