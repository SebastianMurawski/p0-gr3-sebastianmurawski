package pl.edu.uwm.wmii.kotewa.laboratorium00.Labolatorium5;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj rozmiar tablicy: ");
        int rozmiarTablicy =0 ;
        rozmiarTablicy = input.nextInt();
        ArrayList<Integer> arr1 = new ArrayList<Integer>(8);
        for (int i = 0; i < rozmiarTablicy; i++) {
            arr1.add(i, i + 1);
        }
        System.out.println("Poczatkowy wyglad tablicy: ");
        System.out.println(arr1);
        System.out.println("Tablica po odwroceniu: ");
        reverse(arr1);
        System.out.println(arr1);

    }
    public static void reverse(ArrayList<Integer> arr1){
        ArrayList<Integer> odwrocona = new ArrayList<Integer>();
        int rozmiar = arr1.size();
        int j =rozmiar-1;
        for(int i=0;i<rozmiar;i++){
            odwrocona.add(i, arr1.get(j));
            j--;
        }
        for(int i=0;i<rozmiar;i++){
            arr1.set(i,odwrocona.get(i));
        }
    }
}
