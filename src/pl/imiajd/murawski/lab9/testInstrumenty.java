package pl.imiajd.murawski.lab9;
import pl.imiajd.murawski.lab9.Instrument;

import java.time.LocalDate;
import java.util.ArrayList;

public class testInstrumenty {
    public static void main(String[] args) {
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(new Fortepian("YAMAHA", LocalDate.of(2005,01,12)));
        orkiestra.add(new Flet("Fletin",LocalDate.of(2018,11,11)));
        orkiestra.add(new Flet("Fletex",LocalDate.of(2004,12,26)));
        orkiestra.add(new Fortepian("Mason&Hamlin",LocalDate.of(2005,12,1)));
        orkiestra.add(new Skrzypce("YAMAHA",LocalDate.of(2004,12,3)));

        for(Instrument g : orkiestra){
            g.dzwiek();
            System.out.println(g+"\n");
        }
    }
}
