package pl.imiajd.murawski.lab9;

import java.time.LocalDate;

abstract class Osoba
{
    public Osoba(String nazwisko,String[] imiona, boolean plec, LocalDate dataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.plec = plec;
        this.dataUrodzenia=dataUrodzenia;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }
    public String getImiona(){
        return imiona[0]+" "+imiona[1];
    }
    public String getPlec(){
        if(plec){
            return "Mężczyzna";
        }else{
            return "Kobieta";
        }
    }
    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }


    private String nazwisko;
    private String[] imiona;
    private boolean plec;
    private LocalDate dataUrodzenia;
}