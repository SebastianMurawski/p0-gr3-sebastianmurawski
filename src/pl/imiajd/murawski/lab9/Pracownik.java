package pl.imiajd.murawski.lab9;


import pl.imiajd.murawski.lab9.Osoba;

import java.time.LocalDate;

class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, double pobory, String[] imiona, LocalDate dataUrodzenia, boolean plec, LocalDate dataZatrudnienia )
    {
        super(nazwisko,imiona,plec,dataUrodzenia);
        this.pobory = pobory;
        this.dataZatrudnienia=dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("\n    pracownik z pensją %.2f zł\n    Imiona: %s \n    Data urodzenia: %s\n    Płeć: %s \n    Data zatrudnienia: %s", getPobory(),getImiona(),getDataUrodzenia(),getPlec(),getDataZatrudnienia());
    }
    public LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}