package pl.imiajd.murawski.lab9;

import java.time.LocalDate;

public abstract class Instrument {
    private String producent;
    private LocalDate rokProdukcji;

    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent=producent;
        this.rokProdukcji=rokProdukcji;
    }
    abstract void dzwiek();

    public String getProducent(){
        return producent;
    }
    public LocalDate getRokProdukcji(){
        return rokProdukcji;
    }

    public boolean equals(Instrument Object){
        if(this == Object){
            return true;
        }
        if(this.producent.equals(Object.producent)&& this.rokProdukcji==Object.rokProdukcji){
            return true;
        }else{
            return false;
        }
    }
    @Override
    public String toString() {
        return "Producent " + producent + "\nRok Produkcji: " + rokProdukcji;
    }


}
