package pl.imiajd.murawski.lab9;

import java.time.LocalDate;

public class Fortepian extends Instrument {

    public Fortepian(String producent, LocalDate rokProdukcji){
        super(producent,rokProdukcji);
    }
    @Override
    void dzwiek(){
        System.out.println("Dziwek fortepianu~~!!");
    }
}
