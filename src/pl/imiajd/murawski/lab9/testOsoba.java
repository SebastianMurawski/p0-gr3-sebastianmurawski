package pl.imiajd.murawski.lab9;
import pl.imiajd.murawski.lab9.Osoba;

import java.time.LocalDate;

public class testOsoba {
    public static void main(String[] args) {
        Osoba [] people = new Osoba[2];
        people[0] = new Student("Murawski",new String[]{"Sebastian","Andrzej"},LocalDate.of(1999,10,01),true,"Informatyka",4.5);
        people[1]= new Pracownik("Kazimierczak",100000,new String[]{"Joanna","Marta"},LocalDate.of(1950,11,03),false,LocalDate.of(1999,10,11));
        for (Osoba p : people) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }


    }
}
