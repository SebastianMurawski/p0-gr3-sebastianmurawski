package pl.imiajd.murawski.lab9;

import pl.imiajd.murawski.lab9.Osoba;


import java.time.LocalDate;

class Student extends Osoba
{
    public Student(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko,imiona,plec,dataUrodzenia);
        this.kierunek = kierunek;
        this.sredniaOcen=sredniaOcen;
    }

    public String getOpis()
    {
        return "\n    Kierunek studiów : " + kierunek+ "\n    Imiona: "+getImiona()+"\n    Srednia ocen: "+getSredniaOcen()+"\n    Płeć: " +getPlec() +"\n    Kierunek studiow: "+getKierunek()+"\n    Data Urodzenia: "+getDataUrodzenia();
    }
    public double getSredniaOcen(){
        return sredniaOcen;
    }
    public void setSredniaOcen(double sredniaOcen){
        this.sredniaOcen=sredniaOcen;
    }
    public String getKierunek(){
        return kierunek;
    }

    private String kierunek;
    private double sredniaOcen;
}