package pl.imiajd.murawski.lab9;

import java.time.LocalDate;

public class Flet extends Instrument {

    public Flet(String producent, LocalDate rokProdukcji){
        super(producent, rokProdukcji);
    }
    @Override
    void dzwiek(){
        System.out.println("Dzwieki fletu~~!!");
    }
}
