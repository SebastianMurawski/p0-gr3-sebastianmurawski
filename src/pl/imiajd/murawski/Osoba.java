package pl.imiajd.murawski;

public class Osoba {
    private String nazwisko;
    private int rok_urodzenia;

    public Osoba(String nazwisko, int rok_urodzenia) {
        this.nazwisko = nazwisko;
        this.rok_urodzenia = rok_urodzenia;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getRokUrodzenia() {
        return rok_urodzenia;
    }
    @Override
    public String toString() {
        return " Twoje Nazwisko: " + getNazwisko() + " TwojRok Urodzenia: " + getRokUrodzenia();
    }
}