package pl.imiajd.murawski;

public class Adres {
    private int numer_domu;
    private String ulica;
    private int numer_mieszkania;
    private String kod_pocztowy;
    private String miasto;

    public Adres(int numer_domu, String ulica, int numer_mieszkania, String kod_pocztowy, String miasto) {
        this.numer_domu = numer_domu;
        this.ulica=ulica;
        this.numer_mieszkania=numer_mieszkania;
        this.kod_pocztowy=kod_pocztowy;
        this.miasto=miasto;
    }
    public Adres(int numer_domu, String ulica, String kod_pocztowy, String miasto) {
        this.numer_domu = numer_domu;
        this.ulica=ulica;
        this.kod_pocztowy=kod_pocztowy;
        this.miasto=miasto;
    }
    public void pokaz(){
        System.out.println(kod_pocztowy+ "    "+miasto);
        System.out.println(ulica+"     "+numer_domu+"    "+numer_mieszkania);
    }
    public boolean przed(Adres Object){
        if(kod_pocztowy.equals(Object.kod_pocztowy)){
            return true;
        }
        else {
            return false;
        }
    }
}
