package pl.imiajd.murawski;

public class Nauczyciel extends Osoba {
    private int pensja;
    public Nauczyciel(String nazwisko, int rok_urodzenia, int pensja) {
        super(nazwisko, rok_urodzenia);
        this.pensja = pensja;
    }
    public int getPensja() {
        return pensja;
    }
    @Override
    public String toString() {
        return super.toString() + " Twoja pensja: " + pensja;
    }
}

