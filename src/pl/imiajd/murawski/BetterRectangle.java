package pl.imiajd.murawski;

public class BetterRectangle extends java.awt.Rectangle{
    public BetterRectangle(int height , int width){
        super(height,width);
    }
    public double getPerimeter(){
        double obwod;
        obwod = (height+width)*2;
        return obwod;
    }
    public double getArea(){
        double pole;
        pole = height*width;
        return pole;
    }
}
