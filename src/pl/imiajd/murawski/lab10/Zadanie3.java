package pl.imiajd.murawski.lab10;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<String> lista = new ArrayList<>();
        //File pliki = new File(args[0]);
        File pliki = new File("D:\\Studia\\Semestr 3\\JavaProjekty\\src\\pl\\imiajd\\murawski\\lab10\\Slowa.txt");

        Scanner input = new Scanner(pliki);

        while(input.hasNextLine()){
            lista.add(input.nextLine());
        }
        input.close();

        Collections.sort(lista);
        System.out.println(lista);
        System.out.println("\n\n");
        for(String i : lista){
            System.out.println(i);
        }


    }
}
