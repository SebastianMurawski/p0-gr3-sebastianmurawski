package pl.imiajd.murawski.lab10;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba {
    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<>();
        grupa.add(new Osoba("Murawski", LocalDate.of(1999,10,01)));
        grupa.add(new Osoba("Murawski", LocalDate.of(1998,10,01)));
        grupa.add(new Osoba("Nowak", LocalDate.of(2000,10,01)));
        grupa.add(new Osoba("Kowalski", LocalDate.of(2000,10,01)));
        grupa.add(new Osoba("Siemaszko", LocalDate.of(1969,9,01)));

        for (Osoba i : grupa) {
            System.out.println(i);
        }
        System.out.println("\n");
        Collections.sort(grupa);
        for (Osoba i : grupa){
            System.out.println(i);

        }
    }
}
