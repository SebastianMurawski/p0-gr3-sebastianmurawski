package pl.imiajd.murawski.lab10;

import java.time.LocalDate;


public class Student extends Osoba implements Cloneable, Comparable<Osoba> {

    public Student(double sredniaOcen, String nazwisko, LocalDate dataUrodzenia){
        super(nazwisko,dataUrodzenia);
        this.sredniaOcen=sredniaOcen;
    }

    public double getSredniaOcen(){
        return sredniaOcen;
    }

    public int compareTo(Osoba obiekt){
        if(super.compareTo(obiekt)==0){
            return (int)(this.sredniaOcen-((Student)obiekt).sredniaOcen);
        }
        return super.compareTo(obiekt);
    }
    private double sredniaOcen;
}
