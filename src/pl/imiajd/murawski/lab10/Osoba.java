package pl.imiajd.murawski.lab10;

import java.time.LocalDate;

public class Osoba implements Cloneable,Comparable<Osoba>{

    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }
    public String getNazwisko(){
        return nazwisko;
    }
    public Osoba(String nazwisko, LocalDate dataUrodzenia){
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }
    @Override
    public String toString() {
        String temp = String.format(" [Nazwisko: %s  %s]",getNazwisko(),getDataUrodzenia());
        return temp;
    }
    @Override
    public boolean equals(Object Obiekt){
        Osoba temp = (Osoba) Obiekt;
        if(this.nazwisko.equals(temp.nazwisko)&& this.dataUrodzenia.equals(temp.dataUrodzenia)){
            return true;
        }
        return false;
    }
    @Override
    public int compareTo(Osoba Obiekt){
        if(this.nazwisko.compareTo(Obiekt.nazwisko)==0){
            return this.dataUrodzenia.compareTo(Obiekt.dataUrodzenia);
        }
        return this.nazwisko.compareTo(Obiekt.nazwisko);
    }
    private LocalDate dataUrodzenia;
    private String nazwisko;

}
