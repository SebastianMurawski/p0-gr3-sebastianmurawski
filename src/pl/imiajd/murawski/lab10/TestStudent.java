package pl.imiajd.murawski.lab10;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String[] args) {
        ArrayList<Student> grupa = new ArrayList<>();

        grupa.add(new Student(5.6,"Murawski", LocalDate.of(1999,10,01)));
        grupa.add(new Student(4.2,"Nowak", LocalDate.of(1999,10,01)));
        grupa.add(new Student(3.4,"Andrzejewski", LocalDate.of(1999,10,01)));
        grupa.add(new Student(3,"Suchodolski", LocalDate.of(1999,10,01)));
        grupa.add(new Student(4.4,"Kononowicz", LocalDate.of(1963,1,21)));
        System.out.println("Lista Studnetow:");
        for(Student i : grupa){
            System.out.println(i);;
        }
        Collections.sort(grupa);
        System.out.println("Lista Studentow po sortowaniu:");
        for(Student i : grupa){
            System.out.println(i);
        }
    }
}
